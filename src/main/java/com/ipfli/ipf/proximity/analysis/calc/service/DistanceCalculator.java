package com.ipfli.ipf.proximity.analysis.calc.service;

import com.ipfli.ipf.proximity.analysis.calc.controller.request.DistanceRequest;
import com.ipfli.ipf.proximity.analysis.calc.controller.request.DistanceResponse;
import com.ipfli.ipf.proximity.analysis.calc.controller.request.Location;
import net.sf.geographiclib.Geodesic;
import net.sf.geographiclib.GeodesicData;
import net.sf.geographiclib.GeodesicMask;

import java.util.HashMap;
import java.util.Map;

public class DistanceCalculator {
    public DistanceResponse calculateDistance(DistanceRequest request) {
        DistanceResponse distanceResponse = new DistanceResponse();
        distanceResponse.setPartyA(request.getPartyA());
        distanceResponse.setPartyB(request.getPartyB());

        Map<String, Double> distances = new HashMap<>();
        distances.put("Geodesic", getGeodesicDistance(request));
        distances.put("Plannar", getPlannarDistance(request));
        distanceResponse.setDistance(distances);

        return distanceResponse;
    }

    private double getGeodesicDistance(DistanceRequest request){
        Location locationA = request.getPartyA();
        Location locationB = request.getPartyB();
        GeodesicData geodesicData = Geodesic.WGS84.Inverse(locationA.getLatitude(),
                                                           locationA.getLongitude(),
                                                           locationB.getLatitude(),
                                                           locationB.getLongitude(),
                                                           GeodesicMask.DISTANCE);
        return geodesicData.s12;
    }

    private double getPlannarDistance(DistanceRequest request) {
        Location locationA = request.getPartyA();
        Location locationB = request.getPartyB();
        return PlanarCalculator.getDistance(locationA.getLatitude(),
                                            locationA.getLongitude(),
                                            locationB.getLatitude(),
                                            locationB.getLongitude());
    }
}
