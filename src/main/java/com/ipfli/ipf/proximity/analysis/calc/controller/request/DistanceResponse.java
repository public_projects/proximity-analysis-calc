package com.ipfli.ipf.proximity.analysis.calc.controller.request;

import java.util.Map;

public class DistanceResponse {
    private Location partyA;
    private Location partyB;
    private Map<String, Double> distance;

    public Location getPartyA() {
        return partyA;
    }

    public void setPartyA(Location partyA) {
        this.partyA = partyA;
    }

    public Location getPartyB() {
        return partyB;
    }

    public void setPartyB(Location partyB) {
        this.partyB = partyB;
    }

    public Map<String, Double> getDistance() {
        return distance;
    }

    public void setDistance(Map<String, Double> distance) {
        this.distance = distance;
    }
}
