package com.ipfli.ipf.proximity.analysis.calc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProximityAnalysisCalcApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProximityAnalysisCalcApplication.class, args);
	}

}
