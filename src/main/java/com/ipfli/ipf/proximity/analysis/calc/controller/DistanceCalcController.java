package com.ipfli.ipf.proximity.analysis.calc.controller;

import com.ipfli.ipf.proximity.analysis.calc.controller.request.DistanceRequest;
import com.ipfli.ipf.proximity.analysis.calc.controller.request.DistanceResponse;
import com.ipfli.ipf.proximity.analysis.calc.service.DistanceCalculatorService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DistanceCalcController {

    @Autowired
    private DistanceCalculatorService distanceCalculatorService;

    /**
     * Request body:
     *
     * [
     *     {
     *         "partyA": {
     *             "latitude": 43.506269,
     *             "longitude": 16.476429
     *         },
     *         "partyB": {
     *             "latitude": 43.506269,
     *             "longitude": 16.472720
     *         }
     *     }
     * ]
     *
     * @param distanceRequests
     * @return
     */
    @PostMapping("distance")
    @ResponseBody
    public List<DistanceResponse> getDistance(@RequestBody List<DistanceRequest> distanceRequests) {
        return distanceCalculatorService.calculate(distanceRequests);
    }
}
