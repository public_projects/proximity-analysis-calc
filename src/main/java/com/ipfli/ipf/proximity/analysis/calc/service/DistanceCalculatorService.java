package com.ipfli.ipf.proximity.analysis.calc.service;

import com.ipfli.ipf.proximity.analysis.calc.controller.request.DistanceRequest;
import com.ipfli.ipf.proximity.analysis.calc.controller.request.DistanceResponse;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DistanceCalculatorService {

    public List<DistanceResponse> calculate(List<DistanceRequest> distanceRequests) {
        DistanceCalculator distanceCalculator = new DistanceCalculator();
        List<DistanceResponse> responses = new ArrayList<>();
        for (DistanceRequest distanceRequest : distanceRequests) {
            responses.add(distanceCalculator.calculateDistance(distanceRequest));
        }

        return responses;
    }
}
