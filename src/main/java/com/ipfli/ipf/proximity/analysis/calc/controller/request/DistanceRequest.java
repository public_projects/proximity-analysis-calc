package com.ipfli.ipf.proximity.analysis.calc.controller.request;

public class DistanceRequest {
    private Location partyA;
    private Location partyB;

    public Location getPartyA() {
        return partyA;
    }

    public void setPartyA(Location partyA) {
        this.partyA = partyA;
    }

    public Location getPartyB() {
        return partyB;
    }

    public void setPartyB(Location partyB) {
        this.partyB = partyB;
    }
}
